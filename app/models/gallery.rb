class Gallery < ApplicationRecord
    has_one_attached :image #virtual não precisa estar no banco
    validates :image, presence: true,:allow_nil => false,:allow_blank => false

    rails_admin do
        label "Galeria"
    	label_plural "Galeria"
    	weight 6
    	edit do
      		configure :image do
        		label "Imagem"
      		end
    	end
    	list do
      		field :id do
        		label 'Id'
      		end
      		field :image do
        		label 'Imagem'
      		end
    	end
    end

end
