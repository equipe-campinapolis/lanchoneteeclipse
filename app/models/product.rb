class Product < ApplicationRecord
	belongs_to :product_category
	has_many :pizza_types
  has_many :broth_types
  has_many :appettizer_types
  #accepts_nested_attributes_for :appettizer_types
  #accepts_nested_attributes_for :broth_types
  #accepts_nested_attributes_for :pizza_types


	has_one_attached :image #virtual não precisa estar no banco
	acts_as_taggable

  scope :habilitados, -> { where(disable: false) }
  scope :visiveis_em_receitas, -> { where(display_in_recipe: true) }
  # after_initialize do
  #   if not new_record?
  #   end
  # end

  	rails_admin do
    	label "Produto"
    	label_plural "Produtos"
    	weight 4
      edit do
        include_fields :disable,:display_in_recipe,:product_category,:name,
                       :pizza_types,:broth_types,:appettizer_types,
                       :description,:price,:image,:tag_list
        configure :disable do
          label "Desabilitar"
        end
        configure :display_in_recipe do
          label "Mostar a Receita"
        end
        configure :product_category do
          label "Tipo do Produto"
          def render
            bindings[:view].render partial: "categoria_de_produto",:locals => {:field => self, :form => bindings[:form],:p => partial}
          end
        end
        configure :image do
          label "Imagem do Produto"
      	end
      	configure :name do
          label 'Nome'
      	end
      	configure :description do
          label 'Descrição'
      	end
      	configure :price do
          label 'Preço'
        	partial "price_partial", valor:10
      	end
      	configure :pizza_types do
          label 'Tipos de Pizzas'
          def render
            bindings[:view].render partial: "pizza_types",:locals => {:field => self, :form => bindings[:form],:p => partial}
          end
        end
        configure :broth_types do
          label 'Tipos de Caldos'
          def render
            bindings[:view].render partial: "broth_types",:locals => {:field => self, :form => bindings[:form],:p => partial}
          end
        end
        configure :appettizer_types do
          label 'Tipos de Porções'
          def render
            bindings[:view].render partial: "appettizer_types",:locals => {:field => self, :form => bindings[:form],:p => partial}
          end
        end
      	configure :tag_list do
          partial 'tag_list'
        	ratl_max_suggestions -1
        	label 'Ingredientes'
        	help 'separe os ingredientes por virgula'
      	end
    	end
    	list do
        field :name do label "Produto" end
        field :pizza_types do visible false end
        field :broth_types do visible false end
        field :appettizer_types do visible false end
        field :description do visible false end
        field :price do label "Preço" end
        field :image
        field :tag_list do visible false end
        field :disable do
          label "Habilitado"
          def value
            !bindings[:object].disable
          end
        end
        field :display_in_recipe do label "Aparecer em Receitas" end
      end
	end
end
