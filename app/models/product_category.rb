class ProductCategory < ApplicationRecord
	has_many :products, dependent: :destroy

	rails_admin do
		label "Tipos de Produto"
    	label_plural "Tipos de Produtos"
    	weight 3
    	edit do
      		configure :name do
        		label "Titulo"
      		end
      		configure :products do
        		hide
      		end
    	end
    	list do
      		configure :name do
        		label "Nome"
      		end
      		configure :products do
        		label "Produtos Cadastrados"
      		end
      		field :name
      		field :products
    	end
	end
end
