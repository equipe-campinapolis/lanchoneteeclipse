class About < ApplicationRecord
	before_create :deleta_registro

	def deleta_registro
		About.first.destroy if About.count > 0
	end

	rails_admin do
	    label "Sobre nós"
    	label_plural "Sobre nós"
    	weight 7
    	edit do
      		configure :text do
        		label "Texto sobre nós"
      		end
    	end
    	list do
      		field :text do
        		label "texto"
      		end
      		field :created_at do
        		label "Data da criação"
      		end
      end
    end

end
