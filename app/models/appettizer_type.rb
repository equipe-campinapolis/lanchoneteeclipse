class AppettizerType < ApplicationRecord
	enum size: [:meia,:grande]

	def title
		if self.size == "grande" then
  			title = "porção "+ self.size + " de "+self.price.to_s + " reais"
  		end
  		if self.size == "meia" then
  			title = self.size+ " porção de " +self.price.to_s+ " reais"
  		end
  		title
	end

	rails_admin do
		visible false
		label "Tipo de Porção"
    	label_plural "Tipos de Porções"
    	field :size do
    		label "Tamanho"
    	end
    	field :price do
    		label "Preço"
    		partial "price_porcao"
    	end
    	field :product, :hidden do
      		visible false
      	end
	end
end
