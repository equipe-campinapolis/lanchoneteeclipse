class PizzaType < ApplicationRecord
	enum size: [:brotinho, :pequena,:media,:grande,:familia]

	def title
  		title = "pizza "+ self.size + " de "+self.price.to_s + " reais"
  		title
	end

	rails_admin do
		visible false
		label "Tipo de Pizza"
    	label_plural "Tipos de Pizzas"
    	field :size do
    		label "Tamanho"
    	end
    	field :price do
    		label "Preço"
    		partial "price_pizza"
    	end
    	field :product, :hidden do
      		visible false
      	end
	end
end
