class Information < ApplicationRecord
	has_one_attached :image #virtual não precisa estar no banco
	validates :title, presence: true,:allow_nil => false
	validates :description, presence: true,:allow_nil => false
	validates :image, presence: true,:allow_nil => false

	rails_admin do
	    label "Espaço & Serviços"
    	label_plural "Espaço & Serviços"
    	weight 2
    	edit do
      		configure :image do
        		label "Imagem"
      		end
      		configure :title do
        		label 'Titulo'
      		end
      		configure :description do
        		label 'Descrição'
      		end
    	end
    	list do
      		configure :title do
        		label 'Titulo'
      		end
      		configure :description do
        		label 'Descrição'
      		end
      		configure :image do
        		label 'Imagem'
      		end
      		field :title
      		field :description
      		field :image
    	end
	end
end