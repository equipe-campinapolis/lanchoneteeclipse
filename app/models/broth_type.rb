class BrothType < ApplicationRecord
	enum size: [:meio,:grande]

	def title
		if self.size == "grande" then
  			title = "caldo "+ self.size + " de "+self.price.to_s + " reais"
  		end
  		if self.size == "meio" then
  			title = self.size+ " caldo de " +self.price.to_s+ " reais"
  		end
  		title
	end

	rails_admin do
		visible false
		label "Tipo de Caldo"
    	label_plural "Tipos de Caldos"
    	field :size do
    		label "Tamanho"
    	end
    	field :price do
    		label "Preço"
    		partial "price_caldo"
    	end
    	field :product, :hidden do
      		visible false
      	end
	end
end
