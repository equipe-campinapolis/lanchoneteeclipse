class Celebration < ApplicationRecord
    default_scope { order(data: :asc) }

	rails_admin do
    	label "Comemoração"
    	label_plural "Comemorações"
    	weight 5
    	field :title do
        	label "Titulo"
    	end
    	field :data do
      		label "Data do evento"
    	end
    	field :expiration do
      		label "Expira em"
    	end
	end
end
