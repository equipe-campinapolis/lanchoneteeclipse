class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  enum tipo: [:cliente, :admin]
  enum status: [:ativo, :inativo]

  rails_admin do
	label " Usuário"
    label_plural " Usuários"
    navigation_icon 'icon-user'
    weight 1
    list do
      field :nome
      field :email
      field :tipo
    end
  end

end
