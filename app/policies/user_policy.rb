class UserPolicy
 attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def dashboard?
    true
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    if @user.tipo == "admin"
      true
    else
      false
    end
  end

  def new?
    if user.tipo == "admin"
      true
    else
      false
    end
  end

  def update?
    if user.tipo == "admin"
      true
    else
      false
    end
  end

  def edit?
    if user.tipo == "admin"
      true
    else
      false
    end
  end

  def destroy?
    if user.tipo == "admin"
      true
    else
      false
    end
  end

  def export?
   true
  end

  def show_in_app?
    if user.tipo == "admin"
      true
    else
      false
    end
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
