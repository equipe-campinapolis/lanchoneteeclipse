class HomeController < ApplicationController

  def index
  	@informacoes = Information.all
		@categorias = ProductCategory.all
		@comemoracoes = Celebration.all
		@fotos = Gallery.all
    @sobre = About.last
  	@contador = 0
  end

  def listar_produtos
    id = params[:id].downcase.singularize.downcase
    id = "preparos" if ["preparo"].include?(id)
    if not id == "todo" then
  	  tipo_do_produto = ProductCategory.where(name:id)
      produtos = Product.habilitados.visiveis_em_receitas.where(product_category_id: tipo_do_produto)
  	 @produtos = produtos
    else
      @produtos = Product.visiveis_em_receitas.shuffle[0..8]
    end
  	 render :template => "home/template_cardapio_b"
  end

  def enviar_mensagem
    @contato = Contact.new(parametros_do_contato)
    if @contato.save
      redirect_to root_url, :flash => { :success => "Mensagem enviada com sucesso!" }
    else
      redirect_to root_url, :flash => {:alert => "Erro ao enviar mensagem!"}
    end
  end

  private

  def parametros_do_contato
    params.require(:contact).permit(:name, :email, :message)
  end

end