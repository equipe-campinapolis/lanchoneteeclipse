class PagarController < ApplicationController
  def index
  	@pagamentos = Payment.all
  end

  def processar
    url = request.url
    result = url.split("/")
    if result[5] == "1" and result[6] == "1" and not result[7]["collection_status=approved"].nil?
        if params[:status] == "1"
      		pagamento = Payment.where(monthy:params[:mes]).first
      		pagamento.enum_status = "pago"
      		pagamento.save
      	end
      	if params[:status] == "2"
      		pagamento = Payment.where(monthy:params[:mes]).first
      		pagamento.enum_status = "aguardando"
      		pagamento.save
      	end
    end
    @pagamentos = Payment.all
    @refresh = true
    render :index
  end
end
