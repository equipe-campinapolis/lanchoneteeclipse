require 'test_helper'

class CelebrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @celebration = celebrations(:one)
  end

  test "should get index" do
    get celebrations_url
    assert_response :success
  end

  test "should get new" do
    get new_celebration_url
    assert_response :success
  end

  test "should create celebration" do
    assert_difference('Celebration.count') do
      post celebrations_url, params: { celebration: { data: @celebration.data, expiration: @celebration.expiration, title: @celebration.title } }
    end

    assert_redirected_to celebration_url(Celebration.last)
  end

  test "should show celebration" do
    get celebration_url(@celebration)
    assert_response :success
  end

  test "should get edit" do
    get edit_celebration_url(@celebration)
    assert_response :success
  end

  test "should update celebration" do
    patch celebration_url(@celebration), params: { celebration: { data: @celebration.data, expiration: @celebration.expiration, title: @celebration.title } }
    assert_redirected_to celebration_url(@celebration)
  end

  test "should destroy celebration" do
    assert_difference('Celebration.count', -1) do
      delete celebration_url(@celebration)
    end

    assert_redirected_to celebrations_url
  end
end
