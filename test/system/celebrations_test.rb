require "application_system_test_case"

class CelebrationsTest < ApplicationSystemTestCase
  setup do
    @celebration = celebrations(:one)
  end

  test "visiting the index" do
    visit celebrations_url
    assert_selector "h1", text: "Celebrations"
  end

  test "creating a Celebration" do
    visit celebrations_url
    click_on "New Celebration"

    fill_in "Data", with: @celebration.data
    fill_in "Expiration", with: @celebration.expiration
    fill_in "Title", with: @celebration.title
    click_on "Create Celebration"

    assert_text "Celebration was successfully created"
    click_on "Back"
  end

  test "updating a Celebration" do
    visit celebrations_url
    click_on "Edit", match: :first

    fill_in "Data", with: @celebration.data
    fill_in "Expiration", with: @celebration.expiration
    fill_in "Title", with: @celebration.title
    click_on "Update Celebration"

    assert_text "Celebration was successfully updated"
    click_on "Back"
  end

  test "destroying a Celebration" do
    visit celebrations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Celebration was successfully destroyed"
  end
end
