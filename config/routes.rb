Rails.application.routes.draw do
  get 'pagar/index'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, :skip => [:registrations]
  resources :contacts
  resources :abouts
  resources :galleries
  resources :celebrations
  #resources :informations
  #resources :products
  #resources :product_categories
  root "home#index"
  get "home/listar_produtos", to: "home#listar_produtos"
  get "/pagar", to: "pagar#index"
  get "/pagar/processar/:mes/:status/:p", to: "pagar#processar"
  post "home/enviar_mensagem", to: "home#enviar_mensagem"
  #get 'contact/index'
  #get 'menu/index'
  #get 'gallery/index'
  #get 'about/index'
  #get 'home/index'
end
