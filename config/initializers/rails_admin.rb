include RailsAdminTagList::SuggestionsHelper
require 'i18n'
I18n.default_locale = :'pt-BR'
RailsAdmin.config do |config|
  config.parent_controller = 'ApplicationController' #corrigindo bug do pundit
  config.main_app_name = ["Eclipse Lanchonete", " - Administração"]
  config.excluded_models << "Punch"
  config.included_models << "Information"
  config.included_models << "ProductCategory"
  config.included_models << "Product"
  config.included_models << "Celebration"
  config.included_models << "Gallery"
  config.included_models << "About"
  config.included_models << "User"
  config.included_models << "PizzaType"
  config.included_models << "BrothType"
  config.included_models << "AppettizerType"
  config.included_models << "Payment"
  ActiveRecord::Base.descendants.each do |imodel|
    config.model "#{imodel.name}" do
      list do
        exclude_fields :created_at, :updated_at
      end
    end
  end

  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.authorize_with :pundit
  config.current_user_method(&:current_user)

  config.actions do
    dashboard
    index
    new
    export
    bulk_delete
    show
    edit
    delete
    #show_in_app
  end
end
