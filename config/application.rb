require_relative 'boot'
require 'rails/all'
Bundler.require(*Rails.groups)
module Cmprestaurante
  class Application < Rails::Application
    config.load_defaults 5.2
    I18n.enforce_available_locales = true
    config.i18n.available_locales = 'pt-BR'
  end
end
