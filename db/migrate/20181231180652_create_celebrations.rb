class CreateCelebrations < ActiveRecord::Migration[5.2]
  def change
    create_table :celebrations do |t|
      t.datetime :data
      t.string :title
      t.date :expiration

      t.timestamps
    end
  end
end
