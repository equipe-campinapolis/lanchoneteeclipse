class AddDisplayInRecipesToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :display_in_recipe, :boolean
  end
end
