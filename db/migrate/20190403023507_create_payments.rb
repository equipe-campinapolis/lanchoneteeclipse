class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :year
      t.integer :monthy
      t.decimal :value
      t.integer :enum_status
      t.string :mp_url

      t.timestamps
    end
  end
end
