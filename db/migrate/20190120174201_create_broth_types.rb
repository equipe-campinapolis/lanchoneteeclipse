class CreateBrothTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :broth_types do |t|
      t.integer :size
      t.decimal :price
      t.integer :product_id
      t.timestamps
    end
  end
end
