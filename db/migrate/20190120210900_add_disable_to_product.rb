class AddDisableToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :disable, :boolean
  end
end
