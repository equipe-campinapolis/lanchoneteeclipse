class AddFlagsToProducts < ActiveRecord::Migration[5.2]
  change_table :products do |t|
 	t.integer  :pizza_tamanho_flags, :null => false, :default => 0
 	t.integer  :meio_inteira_tamanho_flags, :null => false, :default => 0
 	t.integer  :mostrar_preco_ingredientes_flags, :null => false, :default => 0
  end
end
