class RemoveTamanhoFlagsFromProducts < ActiveRecord::Migration[5.2]
  def up
  	remove_column :products, :meio_inteira_tamanho_flags
  	remove_column :products, :mostrar_preco_ingredientes_flags
  end
end
